---
title: Redes Sociais APENAS Refletem os "Problemas" da Sociedade? - sobre PL2630/20 e uma Resposta ao Glenn Greenwald

layout: post

date: 2023-05-02 00:00:00 -0300

image: https://www.splcenter.org/sites/default/files/dsc_6853_copy.jpg

---

![Homem segurando imagem do Pepe the Frog](https://www.splcenter.org/sites/default/files/dsc_6853_copy.jpg)

No dia 14/03/2023, o jornalista e advogado estadunidense Glenn Greenwald, conhecido por seu grande trabalho no caso Snowden e na Vaza-Jato, publicou um artigo no jornal Folha de São Paulo, entitulado "**[Redes sociais refletem os problemas sociais, não os causam](https://www1.folha.uol.com.br/colunas/glenn-greenwald/2023/04/redes-sociais-refletem-os-problemas-sociais-nao-os-causam.shtml)**" com o subtítulo "**Acusar plataformas é mais fácil que implementar políticas para prevenir tiroteios em escolas**". Só pelo título já achei necessário ler pois sabia que daí não sairia boa coisa e ... não deu outra.

Antes de mais nada, quero deixar claro que devemos sim reconhecer o trabalho do Glenn, principalmente ao divulgar ao mundo o vazamento de Snowden e na Vaza-Jato, que foram de grande importância para escancarar como opera o império Yankee em suas nações colonizadas, para que tudo ande conforme seus interesses. Esse artigo é um crítica à sua publicação na Folha e as suas posições de liberdade de expressão absoluta que vem adotado recentemente, chegando até a [defender o Monark quando o mesmo defendeu a criação de um partido nazista no Brasil](https://www.brasil247.com/midia/glenn-greenwald-defende-monark-que-teve-conta-retida-apos-se-dizer-simpatizante-de-terroristas-bolsonaristas).

Como resultado desse debate, a PL 2630/2020, conhecida como "PL das Fake News", que estava parada, basicamente, desde 2021, [teve sua urgência aprovada na Câmara dos Deputados](https://www.terra.com.br/noticias/brasil/politica/como-cada-deputado-votou-a-urgencia-da-pl-2630-das-fake-news-confira,6b6ca4a01ede4afff16e62b17cd68fefiyf5xa39.html).

Analisemos, antes de tudo, o texto do Glenn:

## Massacres nas Escolas são "Patologias"?

Glenn começa seu texto afirmando:

> "Uma sociedade assolada por uma [epidemia de tiroteios em escolas](https://www1.folha.uol.com.br/cotidiano/2023/04/sp-registra-279-ameacas-a-escolas-na-semana-em-que-aluno-matou-professora.shtml),
> ameaçados e concretizados, é, por definição, uma que sofre múltiplas e 
> profundas patologias. Crimes desse tipo são tão monstruosos e 
> incompreensíveis que só poderiam surgir de uma doença disseminada e 
> multifacetada: espiritual, política, econômica e social."

Ele já começa com a premissa de que massacres na escola teriam uma raíz patológica - sendo essa onda de ataques uma epidemia - já que seriam crimes "incompreensíveis". Em outras palavras: "a sociedade está doente e precisamos conserta-la".

Mas com que fundamento ele diz isso? Como que esses ataques são **incrompreensíveis**? Ele não cita absolutamente nenhuma fonte e não se dá ao trabalho de pesquisar o mínimo sobre o histórico de quem comete esses crimes. 

Nada na nossa sociedade é incrompreensível em absoluto. Podemos ter pontos de vistas e elaborar análises diferentes para estudar um fato, ou pode até não haver estudos suficientes ainda. Mas simplesmente ignorar que há um explicação embasada na materialidade para determinado fato social é **cinismo**, com todo respeito ao Glenn. É muito conveniente usar isso para fazer uma análise tosca sobre um assunto e para defender um ponto de vista conveniente a ele mesmo.

Os massacres que ocorrem nas escolas tem motivos, na verdade, muito bem estudados, que possam por cultura da violência na educação escolar e parental, ideologia reacionária presente no ambiente familiar e nas mídias, espetacularização de outros massacres, jornal policial ("datenismo cultural"), etc... 

E se ainda levarmos em conta que **todos** os massacres que ocorreram em escolas foram causados por pessoas do sexo masculino, e que em muitos deles os principais alvos foram mulheres, isso ainda levanta muito mais debate. Temos jovens com problemas mentais e de socialização, dependendo da classe até marginalizados. Há ódio de classe. Mas não há consciência, há apenas um espantalho para que essas pessoas possam descontar seus ódios, achando que estão atingindo os causadores de seus sofrimentos. Os movimentos neofascistas como Incel, redpill, alt right, etc... se tratam exatamente disso: **ódio mal direcionado**.

Continua incompreensívo? Obviamente que não. Já compreendemos? Também não, mas temos uma boa direção de estudo com essas informações acima. Ou seja, é perfeitamente compreensivo sim!

## Quais os Culpados?

Agora, Glenn passa a fazer uma analogia em culpar as redes sociais hoje com culpar artistas e videogames nos ataques de Columbine:

> "Quando os tiroteios indiscriminados em escolas chegaram ao conhecimento público nos EUA —com o [massacre de Columbine, em 1999](https://www1.folha.uol.com.br/mundo/2019/04/apos-20-anos-diretor-de-columbine-ainda-recita-nome-das-vitimas-do-massacre.shtml)—
>  não havia redes sociais para culpar."

Novamente, uma argumentação completamente boba, ilógica e desonesta. Primeiro que há um espantalho de que as pessoas estariam culpando as redes sociais pelos massacres. Mas isso sequer é verdade: o que está sendo discutido é a **influência** que as redes sociais têm nisso e[ o fato delas não estarem cooperando de forma alguma em não disseminar conteúdos explícitos que incentivem outros massacres diretamente](https://www.estadao.com.br/link/empresas/por-que-o-twitter-usa-emoji-de-coco-para-responder-sobre-massacre-nas-escolas/).

O Ministério da Justiça e Segurança Pública em nenhum momento culpou as redes sociais por isso. Apenas baixou uma portaria que obrigada as plataformas a fazer uma "análise de risco sistêmico"" e implementar uma "política de moderação ativa", simplemente pelo motivo de que é fato que **todos** os terroristas das escolas usaram as redes sociais para divulgar seus "trabalhos" e se comunicar com outros simpatizantes, e que, ao mesmo tempo, essas mesmas redes sociais, que lucram e lucram em cima disso tudo, não fizeram absolutamente **NADA** para coibir isso. Nem tirar conteúdo explicito do ar tiraram!

Então nessa portaria do Ministério da Justiça há, na verdade, apenas o mínimo do mínimo que se esperaria numa reação do Estado para com empresas que estão, **no mínimo**, auxiliando em alguma coisa a ideologia desses massacres se propagar.

Agora, para além disso, Glenn dispara um chorume de desonestidade que faz qualquer leitor minimamente sensato se perguntar quanto ele recebe da [bancada do like](https://www.intercept.com.br/2021/11/25/google-e-ifood-montam-bancada-do-lie/) para dizer isso. Vejamos:

> "Assim, os políticos naquela época tentaram atribuir a culpa a um carrossel de culpados: música rock, videogames, filmes violentos, o niilismo das letras de Marilyn Manson e uma subcultura gótica conhecida como a máfia dos sobretudos ("the trenchcoat mafia")"

Sim, quando houve o massacre de Columbine, vários políticos, principalmente os conservadores, procuraram culpar algumas coisas que poderiam influenciar individualmente os terroristas. O jogo Doom, por exemplo, foi um grande bode expiatório. [Eric Harris, um dos invasores, era um entusiasta do jogo](https://doom.fandom.com/wiki/Columbine_High_School_massacre). Isso significa que o Doom causou os ataques? Claro que não! Significa que influenciou? Em algo, provável que sim. Significa que influencia de forma viral e massiva esse tipo de ataque? Não há **nenhuma** evidência disso. E quando falamos de política pública, temos que analisar o que de fato pode influenciar pessoas de forma massiva e viral. Doom é um jogo offline, e nem sequer são pessoas que o jogador mata. É evidente que a polêmica em torno disso que o Glenn está falando se deu na maior parte por uma mídia sensacionalista de espetáculo. O Marilyn Manson, por exemplo, que o Glenn cita, de acordo com relatórios posteriores, os terroristas nem sequer eram fãs do artista.

Mas eu pergunto ao Glenn: o Doom foi proibido? Não, e inclusive eu sou um grande fã até hoje! Marilyn Manson pagou alguma multa, foi censurado, parou de fazer shows? ~~Infelizmente~~ Não.

Agora, sabe o que tem capacidade sim de expalhar ideologia de forma planejada a viralisar? Sabe quem tem o poder de induzir comportamento e sentimento nas pessoas? Tenho certteza que o Glenn sabe bem.

## As Redes Sociais e o Extremismo

![](/assets/img/elon.jpg)

Num artigo que escrevi entitulado "[A Internet dos Anos 2020 (parte 2): Lucrando com Manipulação, Ódio e Desinformação](https://guip.dev/2021/12/30/A-Internet-dos-Anos-2020-parte-2.html)", exponho como a as redes sociais, principalmente o Facebook, se utilizam da propagação do ódio e das *fake news* para terem mais lucro. Neste meu post esponho com evidencias, de estudos científicos (alguns deles da própria Meta), de como as redes sociais são sim **causas** de extremismo político de extrema direita, e de como elas se beneficiam com isso. E não só isso, mas como eles sim teriam capacidade de **acabar** com tudo isso, [mas não o faz por falta de interesse.]([4 grandes mentiras que o Facebook contou para você](https://www.intercept.com.br/2021/10/08/4-grandes-mentiras-que-o-facebook-contou-para-voce/))

Alguns artigos nos enlucidam - com evidencias e não achismos - como ocorre essa influência das redes sociais na indução comportamental e no extremismo de direita. Vejamos por exemplo um dos estudos internos da Meta vazados no [Facebook Papers](https://www.intercept.com.br/2021/12/01/facebook-papers-provas-rede-manipulou-voce/), entitulado "*Carol's Journey to QAnon — A Test User Study of Misinfo & Polarization Risks Encountered through Recommendation Systems*", detalhado pela jornalista Tatiane Dias do The Intercept Brasil:

> "Um post publicado na rede interna da empresa em 3 de setembro de 2020 
> cita três estudos e conclui que leva apenas quatro semanas para que um 
> novo usuário que segue só conteúdo recomendado passe a ver mais posts 
> divisivos, polarizadores ou distorcidos. Um mês de Facebook, e uma 
> pessoa razoável vira um radical político, graças ao algoritmo."- Tatiane Dias

No estudo, conclui-se, citando o próprio:

> “O alcance global do Facebook e outras plataformas de mídia permitem 
> uma rápida disseminação dessas teorias por vastas distâncias. (...)”
> 
> “A interatividade, conteúdo que prende a atenção e a 
> habilidade de formar grupos dedicados em algumas redes sociais facilitam
>  o compartilhamento e a manutenção de teorias da conspiração”.

Acima, os pesquisadores estão se referindo ao grupo conspiracionaista qanon, e algumas conspirações envolvendo vacina da covid-19 e do 5G.

Quer mais? Temos!

Um [relatório da ong SumofUs ](https://s3.amazonaws.com/s3.sumofus.org/pdf/SoU_BrazilElections.pdf)mostra como o Facebook lucrou com os atos violentos promovidos por bolsonaristas golpistas no 7 de setembro de 2022.

Na amostra coletada pela SumofUs, foram analisados 36 anúncios problemáticos, sendo 16 envolvendo o 07/09, 10 anúncios de campanha política antes do dia permitido pela legislação eleitoral e outros 10 contendo ataques políticos, fake news e discurso de ódio, a maioria deles contendo *dog whistles*.

![Screenshot 2023-05-02 at 15-04-41 SoU_BrazilElections.pdf.png](/home/gui/Projects/guip.dev/assets/img/Screenshot%202023-05-02%20at%2015-04-41%20SoU_BrazilElections.pdf.png)

Podem parecer poucos, mas cada anúncio desse teve em média cerca de 60 mil "impressões".

Acho que já falei demais sobre a quantidade de estudos e evidências que já temos sobre isso. Então recomendo ler o meu post mencionado acima e deixo mais outras fontes sobre isso abaixo:

[Facebook passa pano para racistas e supremacistas brancos](https://www.intercept.com.br/2019/09/16/facebook-protege-paginas-de-supremacistas-brancos/)

[Facebook Papers: as provas de como a rede manipulou você](https://www.intercept.com.br/2021/12/01/facebook-papers-provas-rede-manipulou-voce/)

[4 grandes mentiras que o Facebook contou para você](https://www.intercept.com.br/2021/10/08/4-grandes-mentiras-que-o-facebook-contou-para-voce/)

[Facebook, Twitter e Google lucram se você sente raiva](https://www.intercept.com.br/2018/10/01/facebook-google-twitter-radicalizacao/)

[Facebook Knew It Was Fueling QAnon](https://www.vice.com/en/article/wx5v4y/facebooks-algorithm-spread-qanon-content-to-new-users)

[Twitter se nega a barrar conteúdo de apologia da violência - 11/04/2023 - Cotidiano - Folha](https://www1.folha.uol.com.br/cotidiano/2023/04/twitter-se-recusa-a-tirar-do-ar-posts-com-apologia-a-violencia-nas-escolas-e-causa-mal-estar.shtml)

[The Facebook Files - WSJ](https://www.wsj.com/articles/the-facebook-files-11631713039)

[https://facebookpapers.com/](https://facebookpapers.com/)

## PL 2630/20: urgente e necessário!

Com todo esse debate, foi aprovado o pedido de urgência da PL 2630/20, que trata de enrijecer as regulações das plataformas digitais, como as redes sociais e os apps de mensagem como WhatsApp e Telegram.

Mesmo com muitos dos artigos desse projeto de lei já terem sido removidos ou alterados, principalmente devido à pressão da [bancada do like](https://www.intercept.com.br/2021/11/25/google-e-ifood-montam-bancada-do-lie/), isso ainda tem causado grande desespero das Big Techs, indicando claramente que o PL está no caminho certo!
<center>
    <img src="/assets/img/google.png" width="300">
    <img src="/assets/img/felipeneto.png" width="300">
</center>
Mesmo estando mais fraca que a original, se devemos critica-la, deve ser só por isso, por não estar tão forte. Já está mais que na hora de termos legislações fortes e rígidas para com as Big Techs, que lucram em cima de ódio, massacres e desestabilizações políticas! 

E que o Glenn entenda que estamos no Brasil, não nos Estados Unidos e seu Bill of Rights fajuto e ultrapassado que permite com que nazistas se expressem livremente!


