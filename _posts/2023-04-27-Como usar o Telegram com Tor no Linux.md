---
title: Como usar o Telegram com o Tor no Linux
image: "https://miro.medium.com/v2/resize:fit:1124/1*_gwHyLV85FWLaUyR-bi_Ew.png"
---

![Medium](https://miro.medium.com/v2/resize:fit:1124/1*_gwHyLV85FWLaUyR-bi_Ew.png)

Novamente, [o Telegram foi bloqueado no Brasil por descumprir uma ordem judicial para entregar dados de neonazistas no Brasil](https://g1.globo.com/tecnologia/noticia/2023/04/26/telegram-sai-do-ar-no-brasil.ghtml).

Aproveitando esse "embalo", como objeto de estudo, pretendo demonstrar como é simples contornar esse bloqueio sem precisar pagar uma VPN, ou utilizar uma de graça que [roube seus dados](https://www.tecmundo.com.br/internet/263037-utilizar-vpn-gratis-realmente-seguro.htm).

Não pretendo entrar hoje no mérito sobre o que é o Tor, sua filosofia, etc...
Este será um tutorial rápido e pragmático para instalar e configurar o Tor corretamente para utilizar no Telegram e conseguir contornar seu bloqueio no Brasil.

Mas antes de começar, quero deixar claro que, a princípio, não há nada de ilegal em utilizar o Tor no Brasil. 
Entretanto, utilizar ele especificamente com o fim de burlar um decreto/lei/etc, pode te trazer complicações eventuais, dependendo a sua exposição.

Então, quero deixar claro que não estou incentivando essa prática e não me responsabilizo por eventuais problemas jurídicos.

Use por conta e risco!

### 1. Download e Instalação

A forma como você deverá baixar e instalar dependerá da sua distribuição, já que cada uma poderá ter um gerenciador de pacotes específico, com nome do pacote correto também podendo ser diferente.

Mas, se você está vendo esse tutorial, provavelmente você usa uma das distribuições que irei mostrar.

##### Debian Based - Debian, Ubuntu, Linux Mint, Deepin, popOS, Kali...

`sudo apt update`

`sudo apt install tor`

##### Fedora

`sudo dnf update`

`sudo dnf install tor`

##### Arch Based - Arch Linux, Manjaro, Endeavouros

`sudo pacman -Sy`

`sudo pacman -S tor`

### 2. Configuração

Com o Tor instalado, agora devemos certificar que esteja configurado corretamente.

O arquivo de configuração do Tor fica em `/etc/tor/torrc`.

Então, abra o arquivo `torrc` com seu editor de texto de preferência.

Ex: `sudo vim /etc/tor/torrc` (você vai precisar de privilégios de superuser para editá-lo)

```editorconfig
ClientOnly 1
SocksPort 9050  # coloque sua porta de preferência (por padrão é 9050)
```

##### Bridges

O lugar onde você vai usar o Tor é uma rede monitorada que não permite seu uso, ou você está num país em que isso é proibído? 

Você pode usar uma *bridge*, que permite ocultar dos adms da rede que você está usando o *proxy*, utilizando um nó oculto. 

Para isso basta conseguir um IP e Porta de um servidor bridge e utilizar as configurações abaixo no arquivo `torrc` **junto com as configurações anteriores**.

Para conseguir um servidor bridge, existem alguns meios, que você pode conferir [no site do Tor Project](https://tb-manual.torproject.org/pt-BR/bridges/#Obtendo%20endere%C3%A7os%20das%20pontes).



no `etc/tor/torrc` adicione:

```editorconfig
UseBridges 1
Bridge 0.0.0.0:9112 # substitua 0.0.0.0 pelo endereço IP do server bridge e a porta pela porta do server, caso não seja 9112

```

### 3. Inicializando o Tor

No terminal, execute o comando:

`sudo systemctl start tor`



Caso queira que ele inicialize com o sistema, execute também:

`sudo systemctl enable tor`



Para conferir se o Tor inicializou corretamente, você pode conferir com o comando:

`systemctl status tor`

se tudo ocorreu bem, você deve ver algo assim:

![](/home/gui/.config/marktext/images/2023-04-27-00-47-32-image.png)



### Configurando o Tor no Telegram Desktop

1. Entre em configurações:
![passo 1](/assets/img/tor1.png)
![passo 2](/assets/img/tor2.png)
2. Vá em avançado, ou "*advanced*":
![passo 3](/assets/img/tor3.png)
3. Configure o *proxy* Tor como SOCKS5, hostname como `127.0.0.1` e a porta que você escolheu anteriormente.
![passo 4](/assets/img/tor4.png)
![passo 5](/assets/img/tor5.png)

Pronto, agora seu Telegram já deverá estar funcionando com o Tor proxy!
